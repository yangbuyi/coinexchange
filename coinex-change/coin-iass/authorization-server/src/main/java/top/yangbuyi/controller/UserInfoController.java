package top.yangbuyi.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @program: coinex-change
 * @ClassName: UserInfoController
 * @create: 2021-01-01 22:07
 * @author: yangshuai
 * @since： JDK1.8
 * @UserInfoController: 用户信息$
 **/


@RestController
@Slf4j
public class UserInfoController {

	/**
	 * @Description: 杨不易个人网址:https://yangbuyi.top
	 * 功能描述:
	 * @Author: yangbuyi
	 */
	@GetMapping(value = "user/info")
	public Principal hello(Principal principal) {
		log.info("获取thread local 存储的用户信息 改为 JWT存储");
		// 使用thread local来实现用户数据存储
		// Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return principal;
	}

}
