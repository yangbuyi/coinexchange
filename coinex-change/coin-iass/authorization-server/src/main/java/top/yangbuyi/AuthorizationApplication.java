package top.yangbuyi;

import io.swagger.annotations.Authorization;
import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: coinex-change
 * @ClassName: AuthorizationApplication
 * @create: 2020-12-20 19:16
 * @author: yangshuai
 * @since： JDK1.8
 * @AuthorizationApplication: 鉴权系统
 **/

@SuppressWarnings("ALL")
@SpringBootApplication
public class AuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorizationApplication.class, args);
	}

}
