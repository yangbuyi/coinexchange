package top.yangbuyi.config;

import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;

/**
 * @program: coinex-change
 * @ClassName: WebConfig
 * @create: 2021-01-01 21:05
 * @author: yangshuai
 * @since： JDK1.8
 * @WebConfig: web安全配置$
 **/

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * @功能描述:安全配置
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/1 21:16
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		// 任何请求都进入到授权管理器当中
		http.authorizeRequests().anyRequest().authenticated();
	}

	/**
	 * @功能描述:将授权管理器存放在ioc容器当中
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/1 21:10
	 */
	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}

	/**
	 * @功能描述:内存当中创建模拟用户
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/1 21:12
	 */
//	@Bean
//	@Override
//	public UserDetailsService userDetailsService() {
//		InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
//		User user = new User("admin", "123456", Arrays.asList(new SimpleGrantedAuthority("Role_Admin")));
//		inMemoryUserDetailsManager.createUser(user);
//		return inMemoryUserDetailsManager;
//	}
	public static void main(String[] args) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String encode = bCryptPasswordEncoder.encode("123456");
		System.out.println(encode);
	}

	/**
	 * @功能描述:加密器
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 11:44
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
