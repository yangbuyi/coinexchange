package top.yangbuyi.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * @program: coinex-change
 * @ClassName: ResourceServerConfig
 * @create: 2021-01-01 22:10
 * @author: yangshuai
 * @since： JDK1.8
 * @ResourceServerConfig: 资源服务器$开启资源服务器
 **/
@EnableResourceServer
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	/**
	 * 使用JWT进行非对称加密： 私钥产生token 公钥验证token
	 *
	 * 生成私钥加密文件 如需替换 请更改 [coinexchange] 即可:
	 * keytool -genkeypair -alias coinexchange -keyalg RSA -keypass coinexchange -keystore coinexchange.jks -validity 365 -storepass coinexchange
	 *
	 * keytool -genkeypair [OPTION]...
	 *
	 * 生成密钥对
	 *
	 * 选项:
	 *
	 *  -alias <alias>                  要处理的条目的别名
	 *  -keyalg <keyalg>                密钥算法名称
	 *  -keysize <keysize>              密钥位大小
	 *  -sigalg <sigalg>                签名算法名称
	 *  -destalias <destalias>          目标别名
	 *  -dname <dname>                  唯一判别名
	 *  -startdate <startdate>          证书有效期开始日期/时间
	 *  -ext <value>                    X.509 扩展
	 *  -validity <valDays>             有效天数
	 *  -keypass <arg>                  密钥口令
	 *  -keystore <keystore>            密钥库名称
	 *  -storepass <arg>                密钥库口令
	 *  -storetype <storetype>          密钥库类型
	 *  -providername <providername>    提供方名称
	 *  -providerclass <providerclass>  提供方类名
	 *  -providerarg <arg>              提供方参数
	 *  -providerpath <pathlist>        提供方类路径
	 *  -v                              详细输出
	 *  -protected                      通过受保护的机制的口令
	 *
	 * 使用 "keytool -help" 获取所有可用命令
	 */

	/**
	 * 生成公钥解密:
	 * keytool -list -rfc --keystore coinexchange.jks | openssl x509 -inform pem -pubkey
	 *
	 * 会让你输入密钥口令: 为私钥加密时 -keypass coinexchange  这个 更改[coinexchange] 即可
	 */

	/**
	 * 解密完毕：
	 *
	 -----BEGIN PUBLIC KEY-----
	 MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1EOhMc1xvvDnT4svzJlX
	 ahaflnIzt5Um7MDiazlYxpYM7Y4x2BExJVQNTRnhzhy8CSpdl435ETR3DV7c3zXs
	 jZEQ6B0pJDc/qBgMngyrOtGjhO26psbj1CcqaL9s63QsLFSZ6MJaaQhRzh6V6zi8
	 jS8BaW+9KwIT421Y2UewuellAnJTwHpkbN2BwybEXf07jCUptVUMB11QhcYPjOoQ
	 9Zrov2LZ+ItJkPmmdLh+lSbPtuoY7oT6x61jwdaxtHSUtHIU4ky0dlV/nIo/nzYZ
	 jQSMX6KRWLl7Qvhxw0tabaG1ZzC7z7VR14Z3IC34LLbDzRolsEjPu09T6q2b327w
	 4wIDAQAB
	 -----END PUBLIC KEY-----
	 -----BEGIN CERTIFICATE-----
	 MIIDczCCAlugAwIBAgIEM/W67zANBgkqhkiG9w0BAQsFADBqMQswCQYDVQQGEwJD
	 TjERMA8GA1UECBMIc2hhbmdoYWkxETAPBgNVBAcTCHNoYW5naGFpMQ0wCwYDVQQK
	 EwRqYXZhMRMwEQYDVQQLEwp5YW5nYnV5aXlhMREwDwYDVQQDEwh5YW5nYnV5aTAe
	 Fw0yMTAxMDExNDIyNDNaFw0yMjAxMDExNDIyNDNaMGoxCzAJBgNVBAYTAkNOMREw
	 DwYDVQQIEwhzaGFuZ2hhaTERMA8GA1UEBxMIc2hhbmdoYWkxDTALBgNVBAoTBGph
	 dmExEzARBgNVBAsTCnlhbmdidXlpeWExETAPBgNVBAMTCHlhbmdidXlpMIIBIjAN
	 BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1EOhMc1xvvDnT4svzJlXahaflnIz
	 t5Um7MDiazlYxpYM7Y4x2BExJVQNTRnhzhy8CSpdl435ETR3DV7c3zXsjZEQ6B0p
	 JDc/qBgMngyrOtGjhO26psbj1CcqaL9s63QsLFSZ6MJaaQhRzh6V6zi8jS8BaW+9
	 KwIT421Y2UewuellAnJTwHpkbN2BwybEXf07jCUptVUMB11QhcYPjOoQ9Zrov2LZ
	 +ItJkPmmdLh+lSbPtuoY7oT6x61jwdaxtHSUtHIU4ky0dlV/nIo/nzYZjQSMX6KR
	 WLl7Qvhxw0tabaG1ZzC7z7VR14Z3IC34LLbDzRolsEjPu09T6q2b327w4wIDAQAB
	 oyEwHzAdBgNVHQ4EFgQULxPJop151wiqboHNRXSPg92KGVUwDQYJKoZIhvcNAQEL
	 BQADggEBADWZY+Zfqg686G0C69YbWzTBUEMUfk54WuEAwVWFW3eTyd0bMIWLiPLN
	 k5IXSKtmjSQ9Y5WwrxI0GoA7MwHudrmO1hFzD0W5GOrNRqREpbCHvrknTIG6cdEh
	 3vy4NGn7PXmJUq+VvM/ZO0YYDoDGGWPq+RtVz5flFlQIY8iZwMD72axhqMxjItNA
	 GAH+l95AsD2Onztf6+GHy1lmVJmegjYb7whfFC76lFyzIVRxdmtQ+zukBuw/IrbK
	 DjPLjKzq8THzYudwI/PpLLTQvb26fN9gWnItam72vykSIWAwszVh49wvVZDxJuZz
	 hbjj5xHoFUC20kpAHWbm3ysgi2uUr70=
	 -----END CERTIFICATE-----

	 */

}
