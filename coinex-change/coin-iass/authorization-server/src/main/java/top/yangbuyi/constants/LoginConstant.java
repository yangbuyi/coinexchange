package top.yangbuyi.constants;

import lombok.extern.slf4j.Slf4j;

/**
 * @program: coinex-change
 * @ClassName: LoginConstant
 * @create: 2021-01-23 15:22
 * @author: yangshuai
 * @since： JDK1.8
 * @LoginConstant: 登陆常量$
 **/

public class LoginConstant {

	/**
	 * @功能描述:管理员登录类型
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:53
	 */
	public static final String ADMIN_TYPE = "admin_type";

	/**
	 * @功能描述:用户/会员登录类型
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:53
	 */
	public static final String MEMBER_TYPE = "member_type";

	/**
	 * @功能描述:管理员查询用户SQL
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:53
	 */
	public static final String QUERY_ADMIN_SQL = "SELECT `id` ,`username`, `password`, `status` FROM sys_user WHERE username = ? ";

	/**
	 * @功能描述:会员用户查询SQL
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 12:05
	 */
	public static final String QUERY_MEMBER_SQL = "SELECT `id`,`password`, `status` FROM `user` WHERE mobile = ? or email = ? ";


	/**
	 * @功能描述:判断用户是否为管理员
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:53
	 */
	public static final String QUERY_ROLE_CODE_SQL =
			"SELECT `code` FROM sys_role LEFT JOIN sys_user_role ON sys_role.id = sys_user_role.role_id WHERE sys_user_role.user_id= ?";

	/**
	 * @功能描述:用户为管理员时：（拥有全部的权限）
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:53
	 */
	public static final String QUERY_ALL_PERMISSIONS = "SELECT `name` FROM sys_privilege";


	/**
	 * @功能描述:普通用户时（通过用户的ID查询用户的角色|权限）
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:54
	 */
	public static final String QUERY_PERMISSION_SQL =
			"SELECT `name` FROM sys_privilege LEFT JOIN sys_role_privilege ON sys_role_privilege.privilege_id = sys_privilege.id LEFT JOIN sys_user_role  ON sys_role_privilege.role_id = sys_user_role.role_id WHERE sys_user_role.user_id = ?";


	/**
	 * @功能描述:超级管理员角色的code值
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/24 10:54
	 */
	public static final String ADMIN_ROLE_CODE = "ROLE_ADMIN";


}


