package top.yangbuyi.filters;

import com.alibaba.fastjson.JSONObject;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * @program: coinex-change
 * @ClassName: JwtCheckTokenFilter
 * @create: 2021-01-03 19:00
 * @author: yangshuai
 * @since： JDK1.8
 * @JwtCheckTokenFilter: 过滤器拦截jwt校验token是否有效$  GlobalFilter全局拦截
 **/

@Component
public class JwtCheckTokenFilter implements GlobalFilter, Ordered {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	/**
	 * @功能描述:配置白名单放行路径
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/4 21:26
	 */
	@Value("${no.require.token.uri:/admin/login}")
	private Set<String> noRequireTokenUris;

	/**
	 * @功能描述:拦截到用户请求操作
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/3 19:01
	 */
	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		// 1.该接口是否需要token才能访问
		if (!isRequireToken(exchange)) {
			// 不需要token 放行 false
			return chain.filter(exchange);
		}
		// 2.取出用户的token
		String token = getUserToken(exchange);
		// 3.判断用户的token 的 是否有效
		if (StringUtils.isEmpty(token)) {
			return buildeNoAuthorizationResult(exchange);
		}
		// 4.redis当中拿取用户token
		Boolean aBoolean = stringRedisTemplate.hasKey(token);
		if (aBoolean != null && aBoolean) {
			// token有效放行
			return chain.filter(exchange);
		}
		// 5.token失效
		return buildeNoAuthorizationResult(exchange);
	}

	/**
	 * @功能描述:判断接口是否需要token
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/4 20:50
	 */
	private boolean isRequireToken(ServerWebExchange exchange) {
		String path = exchange.getRequest().getURI().getPath();
		if (noRequireTokenUris.contains(path)) {
			// 不需要token
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * @功能描述:根据传递的请求头来拿token
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/4 21:04
	 */
	private String getUserToken(ServerWebExchange exchange) {
		// 获取头部Authorization
		String token = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
		// 判断是否为空
		return token == null ? null : token.replace("bearer ", "");
	}

	/**
	 * @功能描述:给用户响应一个没有token的错误
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/4 21:10
	 */
	private Mono<Void> buildeNoAuthorizationResult(ServerWebExchange exchange) {
		// 1.获取响应
		ServerHttpResponse response = exchange.getResponse();
		response.getHeaders().set("Content-Type", "application/json");
		response.setStatusCode(HttpStatus.UNAUTHORIZED);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("code", HttpStatus.UNAUTHORIZED.value());
		jsonObject.put("error", "No Authorization");
		jsonObject.put("errorMsg", "Token is Null or Error");
		// 创建响应数据
		DataBuffer wrap = response.bufferFactory().wrap(jsonObject.toString().getBytes());
		// 返回流
		return response.writeWith(Flux.just(wrap));
	}


	/**
	 * @功能描述:过滤器顺序-小到大
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2021/1/3 19:01
	 */
	@Override
	public int getOrder() {
		return 0;
	}
}
