package top.yangbuyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @program: coinex-change
 * @ClassName: top.yangbuyi.GatewayServerApplication
 * @create: 2020-12-20 19:16
 * @author: yangshuai
 * @since： JDK1.8
 * @AuthorizationApplication: 网关系统
 **/

@SpringBootApplication
@EnableDiscoveryClient
public class GatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayServerApplication.class, args);
	}

}
