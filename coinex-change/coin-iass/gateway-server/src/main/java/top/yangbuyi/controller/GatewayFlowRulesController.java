package top.yangbuyi.controller;

import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.GatewayApiDefinitionManager;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @program: coinex-change
 * @ClassName: GatewayFlowRulesController
 * @create: 2020-12-27 18:07
 * @author: yangshuai
 * @since： JDK1.8
 * @GatewayFlowRulesController: 网关请求$
 **/


@RestController
@Slf4j
public class GatewayFlowRulesController {

	/**
	 * @功能描述:获取当前系统的限流策略
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2020/12/27 18:34
	 */
	@GetMapping("/gateway")
	public Set<GatewayFlowRule> getGatewayFlowRules() {
		return GatewayRuleManager.getRules();
	}

	/**
	 * @功能描述:获取自定义的api限流分组
	 * @Description:
	 * @Author: yangshuai
	 * @Date: 2020/12/27 18:34
	 */
	@GetMapping("/api")
	public Set<ApiDefinition> getApiGroupRules() {
		return GatewayApiDefinitionManager.getApiDefinitions();
	}


}
